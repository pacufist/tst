import random
import os
import shutil
from PIL import Image

from django.core.files import File
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.urls import reverse
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from marketplace.utils import unique_slug_generator
from selections.models import Collection
from categories.models import Category, Subcategory
from hashtags.models import Hashtag
from sellers.models import SellerAccount


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 100000000000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )


class ProductQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

    def featured(self):
        return self.filter(featured=True, active=True)

    def local(self):
        return self.filter(local=True, active=True)

    def best(self):
        return self.filter(best=True, active=True)

    def trending(self):
        return self.filter(trending=True, active=True)

    def search(self, query):
        lookups = (Q(title__icontains=query) |
                   Q(short_description__icontains=query) |
                   Q(full_description__icontains=query) |
                   Q(hashtags__title__icontains=query)
                   )
        return self.filter(lookups).distinct()


class ProductManager(models.Manager):
    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()

    def featured(self):
        return self.get_queryset().featured()

    def local(self):
        return self.get_queryset().local()

    def best(self):
        return self.get_queryset().best()

    def trending(self):
        return self.get_queryset().trending()

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        return self.get_queryset().search(query)


class Product(models.Model):
    seller            = models.ForeignKey(SellerAccount, null=True, blank=True)
    # user              = models.ForeignKey(settings.AUTH_USER_MODEL)
    # managers           = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="managers_products", blank=True)
    title             = models.CharField(max_length=150, verbose_name="Product name")
    SKU               = models.CharField(null=True, blank=True, max_length=150, verbose_name="Product SKU")
    short_specs       = models.CharField(null=True, blank=True, max_length=350, verbose_name="Produc short specification")
    category          = models.ForeignKey(Category, blank=True,
                                        null=True,
                                        related_name="primary_category",
                                        verbose_name="Product category"
                                    )
    subcategory       = models.ForeignKey(Subcategory, blank=True,
                                        null=True,
                                        related_name="primary_subcategory",
                                        verbose_name="Product subcategory"
                                    )
    collection        = models.ManyToManyField(Collection, related_name="primary_collection", blank=True, verbose_name="Product collection")
    slug              = models.SlugField(null=True, blank=True, verbose_name="Product URL", unique=True)
    media             = models.ImageField(
                        blank=True,
                        null=True,
                        upload_to=upload_image_path)
    media_thumbnail   = ImageSpecField(source='media',
                                      processors=[ResizeToFill(275, 275)],
                                      format='JPEG',
                                      options={'quality': 100})
    short_description = models.TextField(null=True, blank=True, verbose_name="Short product description")
    full_description  = models.TextField(verbose_name="Full product description", blank=True, null=True)
    overview          = models.TextField(null=True, blank=True, verbose_name="Overview product description")
    specs             = models.TextField(null=True, blank=True, verbose_name="Tech specs product")
    seo_title         = models.CharField(null=True, blank=True, max_length=150, verbose_name="SEO title")
    seo_description   = models.CharField(null=True, blank=True, verbose_name="SEO description text", max_length=250)
    amazon            = models.URLField(verbose_name="Amazon link", null=True, blank=True, help_text="You can use only one field")
    bestbuy           = models.URLField(verbose_name="BestBuy link", null=True, blank=True)
    official          = models.URLField(verbose_name="Official web-site link", null=True, blank=True,)
    price             = models.DecimalField(decimal_places=2, max_digits=15, default=39.99)
    hashtags          = models.ManyToManyField(Hashtag, blank=True, verbose_name="Product tags")
    active            = models.BooleanField(default=True, verbose_name="Product active")
    featured          = models.BooleanField(default=False, verbose_name="Featured product")
    local             = models.BooleanField(default=False, verbose_name="Local product")
    best              = models.BooleanField(default=False, verbose_name="Best seller product")
    trending          = models.BooleanField(default=False, verbose_name="Trending product")
    publish_date      = models.DateField(auto_now=False, auto_now_add=False)

    objects = ProductManager()

    class Meta:
        db_table            = 'products'
        verbose_name        = "Product"
        verbose_name_plural = "Products"

    def get_absolute_url(self):
        return reverse("products:detail", kwargs={"slug": self.slug})

    def get_edit_url(self):
        return reverse("sellers:products_edit", kwargs={"slug": self.slug})

    def __str__(self):
        return str(self.title)


def product_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(product_pre_save_receiver, sender=Product)


class Image(models.Model):
    product     = models.ForeignKey(Product)
    image       = models.ImageField(upload_to=upload_image_path, blank=True, null=True)

    def __str__(self):
        return self.product.title


class Video(models.Model):
    product     = models.ForeignKey(Product)
    embed_code  = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.product.title

