import random
import os

from django.conf import settings
from django.db import models


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def logo_sellers_path(instance, filename):
    new_filename = random.randint(1, 100000000000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "sellers/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )


class SellerAccount(models.Model):
    user        = models.ForeignKey(settings.AUTH_USER_MODEL)
    logo        = models.ImageField(upload_to=logo_sellers_path, blank=True, null=True)
    managers    = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='manager_sellers', blank=True)
    active      = models.BooleanField(default=False)
    timestamp   = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return str(self.user.username)

    class Meta:
        db_table = 'sellers'
