from django import forms

from .models import Signup, MarketingPreference


class EmailSignupForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={
        "type": "email",
        "name": "email",
        "id": "email",
        "placeholder": "Email"
    }), label="")
    class Meta:
        model = Signup
        fields = ('email',)


class MarketingPreferenceForm(forms.ModelForm):
    subscribed = forms.BooleanField(label='Receive marketing email?', required=False)
    
    class Meta:
        model = MarketingPreference
        fields = [
            'subscribed'
        ]
