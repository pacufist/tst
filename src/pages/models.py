from django.db import models


class MainPage(models.Model):
    seo_title      = models.CharField(null=True, blank=True, max_length=150, verbose_name="Main page SEO title")
    seo_description = models.CharField(max_length=350, blank=True, null=True, verbose_name="Main page SEO description")
    title_first    = models.CharField(null=True, blank=True, max_length=150, verbose_name="First title")
    title_second   = models.CharField(null=True, blank=True, max_length=150, verbose_name="Second title")
    title_third    = models.CharField(null=True, blank=True, max_length=150, verbose_name="Third title")
    title_fourth   = models.CharField(null=True, blank=True, max_length=150, verbose_name="Fourth title")
    content_first  = models.TextField(null=True, blank=True, verbose_name="First content section")
    content_second = models.TextField(null=True, blank=True, verbose_name="Second content section")
    content_third  = models.TextField(null=True, blank=True, verbose_name="Third content section")
    content_fourth = models.TextField(null=True, blank=True, verbose_name="Fourth content section")

    class Meta:
        verbose_name = "Main page"
        verbose_name_plural = "Main page"

    def __str__(self):
        return self.title_first


class AboutPage(models.Model):
    title         = models.CharField(null=True, blank=True, max_length=150, verbose_name="Main title")
    content_first = models.TextField(null=True, blank=True, verbose_name="Main content section")
    # seo_title      = models.CharField(null=True, blank=True, max_length=150, verbose_name="About page SEO title")
    # seo_description = models.CharField(max_length=350, blank=True, null=True, verbose_name="About page SEO description")

    class Meta:
        verbose_name = "About page"
        verbose_name_plural = "About page"

    def __str__(self):
        return str(self.title)


class ContactPage(models.Model):
    title         = models.CharField(null=True, blank=True, max_length=150, verbose_name="Main title")
    content_first = models.TextField(null=True, blank=True, verbose_name="Main content section")

    class Meta:
        verbose_name = "Contact page"
        verbose_name_plural = "Contact page"

    def __str__(self):
        return self.title
