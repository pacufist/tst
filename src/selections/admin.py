from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin


from .models import Collection


class CollectionAdmin(SummernoteModelAdmin):
    list_display = ['__str__', 'title', 'slug']
    summernote_fields = ('seo_text')

    class Meta:
        model = Collection


admin.site.register(Collection, CollectionAdmin)
