import random
import os
from django.db import models
from django.db.models.signals import pre_save

from marketplace.utils import unique_slug_generator


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 1000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "collections/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )


class CollectionQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


class CollectionManager(models.Manager):
    def all(self):
        return self.get_queryset().all().active()

    def get_queryset(self):
        return CollectionQuerySet(self.model, using=self.db)

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None


class Collection(models.Model):
    title             = models.CharField(max_length=300, verbose_name="Product collection")
    slug              = models.SlugField(null=True, blank=True, verbose_name="Collection category URL", unique=True)
    short_description = models.CharField(max_length=400, verbose_name="Collection short description")
    full_description  = models.TextField(verbose_name="Collection full description")
    seo_title         = models.CharField(null=True, blank=True, max_length=150, verbose_name="Collection SEO title")
    seo_text          = models.TextField(null=True, blank=True, verbose_name="Collection SEO text")
    seo_description   = models.CharField(max_length=350, blank=True, null=True, verbose_name="Collection SEO description")
    image             = models.ImageField(upload_to=upload_image_path, null=True, blank=True, verbose_name="Collection photo")
    active            = models.BooleanField(default=True, verbose_name="Collection active")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/collections/{slug}/".format(slug=self.slug)

    class Meta:
        db_table            = 'collections'
        verbose_name        = "Product collection"
        verbose_name_plural = "Product collections"


def collection_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(collection_pre_save_receiver, sender=Collection)
