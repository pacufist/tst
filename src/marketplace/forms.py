from django import forms
from django.contrib.auth import get_user_model
from django.forms.widgets import CheckboxSelectMultiple
from django_summernote.widgets import SummernoteWidget

from products.models import Product
from categories.models import Category, Subcategory
from categories.models import Subcategory
from selections.models import Collection

User = get_user_model()


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 100000000000000000000)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
        )

class ProductAddForm(forms.Form):
    title           = forms.CharField(
        label="",
        widget=forms.TextInput(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter product name"
                }
            )
        )
    overview        = forms.CharField(widget=SummernoteWidget())
    short_description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter short description of product"
                }
            )
        )
    full_description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter full description of product"
                }
            )
        )
    category        = forms.ModelMultipleChoiceField(
            Category.objects.all(),
            widget=forms.CheckboxSelectMultiple(
                attrs={
                "type": "checkbox",
                }
            )
        )
    subcategory     = forms.ModelMultipleChoiceField(
            Subcategory.objects.all(),
            widget=forms.CheckboxSelectMultiple(
                attrs={
                    "type": "checkbox",
                }
            )
        )
    collection      = forms.ModelMultipleChoiceField(
            Collection.objects.all(),
            widget=forms.CheckboxSelectMultiple(
                attrs={
                    "type": "checkbox",
                }
            )
        )
    amazon = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Amazon link"
                }
            )
        )
    bestbuy = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Bestbuy link"
                }
            )
        )
    official = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Official web-site link"
                }
            )
        )
    full_description = forms.CharField()
    overview        = forms.CharField(widget=SummernoteWidget())
    amazon          = forms.URLField()
    bestbuy         = forms.URLField()
    official        = forms.URLField()
    price           = forms.DecimalField()
    active          = forms.BooleanField()
    publish_date    = forms.DateField()


class ProductModelForm(forms.ModelForm):
    title           = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter product name"
                }
            )
        )
    overview        = forms.CharField(required=False, widget=SummernoteWidget())
    short_description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter short description of product"
                }
            )
        )
    full_description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter full description of product"
                }
            )
        )
    category        = forms.ModelMultipleChoiceField(
            Category.objects.all(),
            required=False,
            widget=forms.CheckboxSelectMultiple(
                attrs={
                "type": "checkbox",
                }
            )
        )
    subcategory     = forms.ModelMultipleChoiceField(
            Subcategory.objects.all(),
            required=False,
            widget=forms.CheckboxSelectMultiple(
                attrs={
                    "type": "checkbox",
                }
            )
        )
    collection      = forms.ModelMultipleChoiceField(
            Collection.objects.all(),
            required=False,
            widget=forms.CheckboxSelectMultiple(
                attrs={
                    "type": "checkbox",
                }
            )
        )
    amazon = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Amazon link"
                }
            )
        )
    bestbuy = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Bestbuy link"
                }
            )
        )
    official = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Official web-site link"
                }
            )
        )
    price = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Price"
                }
            )
        )

    active = forms.BooleanField(
        required=False,
        widget=forms.NullBooleanSelect(
            attrs={
                "class": "form-control",
                }
            )
        )

    publish_date = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Publish date"
                }
            )
        )

    class Meta:
        model = Product
        fields = [
            "title",
            "category",
            "subcategory",
            "collection",
            "short_description",
            "full_description",
            "overview",
            "amazon",
            "bestbuy",
            "official",
            "price",
            "active",
            "publish_date",
        ]

    def clean(self, *args, **kwargs):
        cleaned_data = super(ProductModelForm, self).clean(*args, **kwargs)
        return cleaned_data


class ContactForm(forms.Form):
    fullname = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter your name"
                }
            )
        )
    email    = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control mt-3",
                "placeholder": "Enter your email"
                }
            )
        )
    content  = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder": "Enter your text"
                }
            )
        )
    def clean_email(self):
        email = self.cleaned_data.get("email")
        if not "gmail.com" in email:
            raise forms.ValidationError("Email must be gmail.com")
        return email
