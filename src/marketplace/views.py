from django.contrib.auth import authenticate, login, get_user_model
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from categories.models import Category
from marketing.models import Slider
from pages.models import MainPage, AboutPage
from products.models import Product
from selections.models import Collection


from .forms import ContactForm
from marketing.forms import EmailSignupForm


class HomeView(TemplateView):
    template_name = "index.html"


    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        qs = Product.objects.all().prefetch_related('primary_category')[:8]
        block_products  = Product.objects.all().order_by("?")[:8]
        featured_products = Product.objects.featured()[:4]
        local_products  = Product.objects.local()[:4]
        best_products   = Product.objects.best()[:4]
        trending_products = Product.objects.trending()[:4]
        categories      = Category.objects.all()
        collections     = Collection.objects.all().order_by("?")
        main_slider     = Slider.objects.all()
        seo_block       = MainPage.objects.all()
        form = EmailSignupForm()
        context['form'] = form
        context['block_products'] = block_products
        context['featured_products'] = featured_products
        context['local_products'] = local_products
        context['best_products'] = best_products
        context['trending_products'] = trending_products
        context['collections'] = collections
        context['categories']  = categories
        context['main_slider'] = main_slider
        context['seo_block'] = seo_block
        context ['products'] = qs
        return context


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, *args, **kwargs):
        context = super(AboutView, self).get_context_data(*args, **kwargs)
        return context


class TermsView(TemplateView):
    template_name = "terms-of-use.html"

    def get_context_data(self, *args, **kwargs):
        context = super(TermsView, self).get_context_data(*args, **kwargs)
        return context


class PolicyView(TemplateView):
    template_name = "policy.html"

    def get_context_data(self, *args, **kwargs):
        context = super(PolicyView, self).get_context_data(*args, **kwargs)
        return context


def contact(request):
    contact_form = ContactForm(request.POST or None)
    categories   = Category.objects.all()
    collections  = Collection.objects.all()
    context = {
        "form": contact_form,
        "categories": categories,
        "collections": collections,
    }
    if contact_form.is_valid():
        print(contact_form.cleaned_data)
    return render (request, "contact/view.html", context)
