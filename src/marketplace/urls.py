from django.conf.urls import url, include
from django.contrib.sitemaps.views import sitemap
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView, RedirectView

from django.conf import settings
from django.conf.urls.static import static

from accounts.views import LoginView, RegisterView, guest_register_view
from addresses.views import checkout_address_create_view
from .views import HomeView, AboutView, TermsView, PolicyView, contact
from marketing.views import email_list_signup, MarketingPreferenceUpdateView
from .sitemaps import (
    CategorySitemap,
    CollectionSitemap,
    SubcategorySitemap,
    ProductSitemap,
    StaticViewSitemap
)

sitemaps = {
    'categories': CategorySitemap,
    'collections': CollectionSitemap,
    'subcategories': SubcategorySitemap,
    'products': ProductSitemap,
    'pages': StaticViewSitemap,
}


urlpatterns = [
    # url(r'^$', home, name='home'),
    url(r'^$', HomeView.as_view(), name='index'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^accounts/$', RedirectView.as_view(url='/account')),
    url(r'^account/', include("accounts.urls", namespace='account')),
    url(r'^accounts/', include("accounts.passwords.urls")),
    url(r'^settings/$', RedirectView.as_view(url='/account')),
    url(r'^terms/$', TermsView.as_view(), name='terms'),
    url(r'^policy/$', PolicyView.as_view(), name='policy'),
    url(r'sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    url(r'^products/', include("products.urls", namespace='products')),
    url(r'^subcategories/', include("categories.sub_urls", namespace='subcategories')),
    url(r'^categories/', include("categories.urls", namespace='categories')),
    url(r'^collections/', include("selections.urls", namespace='collections')),
    url(r'^search/', include("search.urls", namespace='search')),
    url(r'^cart/', include("carts.urls", namespace='cart')),
    url(r'^contact/$', contact, name='contact'),
    url(r'^subscribe/$', email_list_signup, name='subscribe'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^checkout/address/create/$', checkout_address_create_view, name='checkout_address_create_view'),
    url(r'^register/guest/$', guest_register_view, name='guest_register'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^settings/email/$', MarketingPreferenceUpdateView.as_view(), name='marketing-pref'),
    url(r'^seller/', include("sellers.urls", namespace='sellers')),
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
