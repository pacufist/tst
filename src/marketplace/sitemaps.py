from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from products.models import Product
from categories.models import Category, Subcategory
from selections.models import Collection


class CategorySitemap(Sitemap):
    changefreq = "daily"
    priority = 0.9

    def items(self):
        return Category.objects.all()


class CollectionSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.9

    def items(self):
        return Collection.objects.all()


class SubcategorySitemap(Sitemap):
    changefreq = "daily"
    priority = 0.8

    def items(self):
        return Subcategory.objects.all()


class ProductSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.6

    def items(self):
        return Product.objects.all()


class StaticViewSitemap(Sitemap):

    def items(self):
        return ['about', 'index']

    def location(self, item):
        return reverse(item)
