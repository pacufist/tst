from django.conf import settings
from django.db import models
from django.db.models.signals import post_save

from accounts.models import GuestEmail
from products.models import Product

User = settings.AUTH_USER_MODEL


class Transaction(models.Model):
    user      = models.ForeignKey(settings.AUTH_USER_MODEL)
    product   = models.ForeignKey(Product)
    price     = models.DecimalField(decimal_places=2, max_digits=15, default=39.99, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    succes    = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)


class BillingProfileManager(models.Manager):
    def new_or_get(self, request):
        user = request.user
        guest_email_id = request.session.get('guest_email_id')
        created = False
        obj = None
        if user.is_authenticated():
            obj, created = self.model.objects.get_or_create(user=user, email=user.email)
        elif guest_email_id is not None:
            guest_email_obj = GuestEmail.objects.get(id=guest_email_id)
            obj, created = self.model.objects.get_or_create(email=guest_email_obj.email)
        else:
            pass
        return obj, created


class BillingProfile(models.Model):
    user      = models.OneToOneField(User, null=True, blank=True)
    email     = models.EmailField()
    active    = models.BooleanField(default=True)
    update    = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    objects   = BillingProfileManager()

    def __str__(self):
        return self.email


def user_created_receiver(sender, instance, created, *args, **kwargs):
    if created and instance.email:
        BillingProfile.objects.get_or_create(user=instance, email=instance.email)

post_save.connect(user_created_receiver, sender=User)
