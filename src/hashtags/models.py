from django.db import models
from django.db.models.signals import pre_save

from marketplace.utils import unique_slug_generator


class Hashtag(models.Model):
    title  = models.CharField(max_length=120)
    slug   = models.SlugField(blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table            = 'hashtags'
        verbose_name        = "Product tag"
        verbose_name_plural = "Product tags"


def tag_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save .connect(tag_pre_save_receiver, sender=Hashtag)
