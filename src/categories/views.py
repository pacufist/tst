from django.views.generic import ListView, DetailView

from products.models import Product
from .models import Category, Subcategory
from selections.models import Collection


class CategoryListView(ListView):
    queryset = Category.objects.all()
    template_name = "categories/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CategoryListView, self).get_context_data(*args, **kwargs)
        qs = Product.objects.prefetch_related()
        context['products'] = qs
        return context


class CategoryDetailSlugView(DetailView):
    queryset = Category.objects.all()
    template_name = "categories/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CategoryDetailSlugView, self).get_context_data(*args, **kwargs)
        qs = self.get_object().primary_category.all()
        categories    = Category.objects.all().prefetch_related('primary_category')
        collections   = Collection.objects.all()
        subcategories   = self.get_object().parent_category.all()
        context['categories']  = categories
        context['collections'] = collections
        context['subcategories'] = subcategories
        context['products'] = qs
        return context


class SubcategoryListView(ListView):
    template_name = "subcategories/list.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Subcategory.objects.all()

    def get_object(self, *args, **kwargs):
        qs = Subcategory.objects.all()
        return qs.first()

    def get_context_data(self, *args, **kwargs):
        context = super(SubcategoryListView, self).get_context_data(*args, **kwargs)
        qs = self.get_object().primary_subcategory.all()
        categories  = Subcategory.objects.all().prefetch_related('primary_subcategory')
        collections = Collection.objects.all()
        context['categories']  = categories
        context['collections'] = collections
        context['products'] = qs
        return context


class SubcategoryDetailSlugView(DetailView):
    queryset = Subcategory.objects.all()
    template_name = "subcategories/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(SubcategoryDetailSlugView, self).get_context_data(*args, **kwargs)
        qs = self.get_object().primary_subcategory.all()
        categories = Category.objects.all().prefetch_related('primary_category')[:2]
        subcategories = Subcategory.objects.all().prefetch_related('primary_subcategory')
        collections = Collection.objects.all()
        context['categories']  = categories
        context['collections'] = collections
        context['products'] = qs
        return context
